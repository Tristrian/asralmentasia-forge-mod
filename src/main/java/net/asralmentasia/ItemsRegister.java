/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia;

import net.asralmentasia.item.*;
import net.asralmentasia.item.cep.ItemCepGrapes;
import net.asralmentasia.item.cep.ItemCepTomato;
import net.asralmentasia.item.seed.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlockSpecial;

import net.minecraftforge.registries.IForgeRegistry;

/**
 *
 * @author Tiago
 */
public class ItemsRegister {

    public static ItemSeedBarley barley_seeds = new ItemSeedBarley();
    public static ItemSeedLettuce lettuce_seeds = new ItemSeedLettuce();
    public static ItemBase barley = new ItemBase("barley");
    public static ItemBase lettuce = new ItemBase("lettuce");
    public static ItemSeedFoodTurnip turnip = new ItemSeedFoodTurnip();
    public static ItemSeedFlax flax_seeds = new ItemSeedFlax();
    public static ItemBase flax = new ItemBase("flax");
    public static ItemSeedHemp hemp_seeds = new ItemSeedHemp();
    public static ItemBase hemp = new ItemBase("hemp");
    public static ItemCepGrapes grapes_ceps = new ItemCepGrapes();
    public static ItemBase grapes_seeds = new ItemBase("grapes_seeds");
    public static ItemBase grapes = new ItemBase("grapes");
    public static ItemCepTomato tomato_ceps = new ItemCepTomato();
    public static ItemBase tomato_seeds = new ItemBase("tomato_seeds");
    public static ItemBase tomato = new ItemBase("tomato");
    public static ItemBlockSpecialAsral cheese = new ItemBlockSpecialAsral(BlocksRegister.cheese_wheel, "cheese");
    public static ItemBase cheese_slice = new ItemBase("cheese_slice");
    public static ItemBlockSpecialAsral bluecheese = new ItemBlockSpecialAsral(BlocksRegister.bluecheese_wheel, "bluecheese");
    public static ItemBase bluecheese_slice = new ItemBase("bluecheese_slice");

    public static void register(IForgeRegistry<Item> registry) {
        registry.registerAll(barley_seeds,
                lettuce_seeds,
                barley,
                lettuce,
                turnip,
                flax_seeds,
                flax,
                hemp_seeds,
                hemp,
                tomato_ceps,
                tomato_seeds,
                tomato,
                grapes_ceps,
                grapes_seeds,
                grapes,
                cheese,
                cheese_slice,
                bluecheese,
                bluecheese_slice
        );

    }

    public static void registerModels() {
        barley_seeds.registerItemModel();
        barley.registerItemModel();
        lettuce_seeds.registerItemModel();
        lettuce.registerItemModel();
        turnip.registerItemModel();
        flax_seeds.registerItemModel();
        flax.registerItemModel();
        hemp_seeds.registerItemModel();
        hemp.registerItemModel();
        tomato_ceps.registerItemModel();
        tomato_seeds.registerItemModel();
        tomato.registerItemModel();
        grapes_ceps.registerItemModel();
        grapes_seeds.registerItemModel();
        grapes.registerItemModel();
        cheese.registerItemModel();
        cheese_slice.registerItemModel();
        bluecheese.registerItemModel();
        bluecheese_slice.registerItemModel();
    }
}
