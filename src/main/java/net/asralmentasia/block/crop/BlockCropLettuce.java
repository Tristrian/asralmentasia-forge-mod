/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia.block.crop;

import net.asralmentasia.ItemsRegister;
import net.minecraft.block.BlockCrops;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 *
 * @author Tiago
 */
public class BlockCropLettuce extends BlockCrops {
        public BlockCropLettuce() {
        setUnlocalizedName("lettuce_crops");
        setRegistryName("lettuce_crops");
        setCreativeTab(CreativeTabs.FOOD);
    }

    @Override
    protected Item getSeed() {
        return ItemsRegister.lettuce_seeds;
    }

    @Override
    protected Item getCrop() {
       return ItemsRegister.lettuce;
    }
}
