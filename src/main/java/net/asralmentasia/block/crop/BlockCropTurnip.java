/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia.block.crop;

import net.asralmentasia.ItemsRegister;
import net.minecraft.block.BlockCrops;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 *
 * @author Tiago
 */
public class BlockCropTurnip extends BlockCrops {

    public BlockCropTurnip() {
        setUnlocalizedName("turnip_crops");
        setRegistryName("turnip_crops");
        setCreativeTab(CreativeTabs.FOOD);
    }

    @Override
    protected Item getSeed() {
        return ItemsRegister.turnip;
    }

    @Override
    protected Item getCrop() {
        return ItemsRegister.turnip;
    }
}
