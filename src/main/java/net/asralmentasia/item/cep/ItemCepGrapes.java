/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia.item.cep;

import net.asralmentasia.BlocksRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemSeeds;

/**
 *
 * @author Tiago
 */
public class ItemCepGrapes extends ItemSeeds {

    public ItemCepGrapes() {
        super(BlocksRegister.grapes_vines, Blocks.FARMLAND);
        setUnlocalizedName("grapes_ceps");
        setRegistryName("grapes_ceps");
        setCreativeTab(CreativeTabs.FOOD);
    }

    public void registerItemModel() {
        net.asralmentasia.Asralmentasia.proxy.registerItemRenderer(this, 0, "grapes_ceps");
    }
}
