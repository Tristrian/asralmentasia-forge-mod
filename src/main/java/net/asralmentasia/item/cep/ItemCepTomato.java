/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia.item.cep;

import net.asralmentasia.BlocksRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemSeeds;

/**
 *
 * @author Tiago
 */
public class ItemCepTomato extends ItemSeeds {

    public ItemCepTomato() {
        super(BlocksRegister.tomato_vines, Blocks.FARMLAND);
        setUnlocalizedName("tomato_ceps");
        setRegistryName("tomato_ceps");
        setCreativeTab(CreativeTabs.FOOD);
    }

    public void registerItemModel() {
        net.asralmentasia.Asralmentasia.proxy.registerItemRenderer(this, 0, "tomato_ceps");
    }
}
