/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia.item;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlockSpecial;

/**
 *
 * @author Tiago
 */
public class ItemBlockSpecialAsral extends ItemBlockSpecial {

    protected String name;

    public ItemBlockSpecialAsral(Block block, String name) {
        super(block);
        this.name = name;
        setUnlocalizedName(name);
        setRegistryName(name);
    }

    public void registerItemModel() {
        net.asralmentasia.Asralmentasia.proxy.registerItemRenderer(this, 0, name);
    }
}
