/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia.item;

import net.minecraft.item.Item;

/**
 *
 * @author Tiago
 */
public class ItemBase extends Item {

    protected String name;

    public ItemBase(String name) {
        this.name = name;
        setUnlocalizedName(name);
        setRegistryName(name);
    }

    public void registerItemModel() {
        net.asralmentasia.Asralmentasia.proxy.registerItemRenderer(this, 0, name);
    }

}
