/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia.item.seed;

import net.asralmentasia.BlocksRegister;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemSeeds;

/**
 *
 * @author Tiago
 */
public class ItemSeedLettuce extends ItemSeeds {

    public ItemSeedLettuce() {
        super(BlocksRegister.lettuce_crops, Blocks.FARMLAND);
        setUnlocalizedName("lettuce_seeds");
        setRegistryName("lettuce_seeds");
        setCreativeTab(CreativeTabs.FOOD);
    }

    public void registerItemModel() {
        net.asralmentasia.Asralmentasia.proxy.registerItemRenderer(this, 0, "lettuce_seeds");
    }
}
