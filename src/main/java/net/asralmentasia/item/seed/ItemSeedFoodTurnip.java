/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia.item.seed;

import net.asralmentasia.BlocksRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemSeedFood;

/**
 *
 * @author Tiago
 */
public class ItemSeedFoodTurnip extends ItemSeedFood {

    public ItemSeedFoodTurnip() {
        super(4, 0.3F, BlocksRegister.turnip_crops, Blocks.FARMLAND);
        setUnlocalizedName("turnip");
        setRegistryName("turnip");
        setCreativeTab(CreativeTabs.FOOD);
    }

    public void registerItemModel() {
        net.asralmentasia.Asralmentasia.proxy.registerItemRenderer(this, 0, "turnip");
    }
}
