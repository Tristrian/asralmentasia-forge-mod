/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia.event;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.ArrowNockEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 *
 * @author Tiago
 */
@Mod.EventBusSubscriber
public class EventBlockDrop {

    @SubscribeEvent
    public static void onBlockDrop(HarvestDropsEvent event) {
        if (event.getState().getBlock() == Block.getBlockById(103)) {
            event.getDrops().clear();
            ItemStack stack = new ItemStack(Block.getBlockById(103));
            event.getDrops().add(stack);
        }
    }

}
