/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.asralmentasia;

import net.asralmentasia.block.cake.BlockCakeCheese;
import net.asralmentasia.block.*;
import net.asralmentasia.block.cake.BlockCakeBlueCheese;
import net.asralmentasia.block.crop.*;
import net.asralmentasia.block.crop.vine.BlockVineGrapes;
import net.asralmentasia.block.crop.vine.BlockVineTomato;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraftforge.registries.IForgeRegistry;

/**
 *
 * @author Tiago
 */
public class BlocksRegister {

    public static BlockBase stuc = new BlockBase(Material.SAND, "stuc");
    public static BlockCropBarley barley_crops = new BlockCropBarley();
    public static BlockCropLettuce lettuce_crops = new BlockCropLettuce();
    public static BlockCropTurnip turnip_crops = new BlockCropTurnip();
    public static BlockCropFlax flax_crops = new BlockCropFlax();
    public static BlockCropHemp hemp_crops = new BlockCropHemp();
    public static BlockVineGrapes grapes_vines = new BlockVineGrapes();
    public static BlockVineTomato tomato_vines = new BlockVineTomato();

    public static BlockSlowHalf mud_half = new BlockSlowHalf(Material.GROUND, "mud_half");
    public static BlockSlowFull mud_full = new BlockSlowFull(Material.GROUND, "mud_full");

    public static BlockCakeCheese cheese_wheel = new BlockCakeCheese();
    public static BlockCakeBlueCheese bluecheese_wheel = new BlockCakeBlueCheese();

    public static void register(IForgeRegistry<Block> registry) {
        registry.register(stuc);
        registry.register(barley_crops);
        registry.register(lettuce_crops);
        registry.register(turnip_crops);
        registry.register(flax_crops);
        registry.register(hemp_crops);
        registry.register(grapes_vines);
        registry.register(tomato_vines);
        registry.register(mud_half);
        registry.register(mud_full);
        registry.register(cheese_wheel);
        registry.register(bluecheese_wheel);
    }

    public static void registerItemBlocks(IForgeRegistry<Item> registry) {
        registry.registerAll(
                stuc.createItemBlock(),
                mud_half.createItemBlock(),
                mud_full.createItemBlock()
        );
    }

    public static void registerModels() {
        stuc.registerItemModel(Item.getItemFromBlock(stuc));
        mud_half.registerItemModel(Item.getItemFromBlock(mud_half));
        mud_full.registerItemModel(Item.getItemFromBlock(mud_full));
    }
}
